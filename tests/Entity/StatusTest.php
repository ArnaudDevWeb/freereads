<?php

namespace App\Tests\Entity;

use App\Entity\Status;
use App\Entity\UserBook;
use PHPUnit\Framework\TestCase;

class StatusTest extends TestCase
{
    public function testGetUserBooks()
    {
        $status = new Status();
        $userBook1 = new UserBook();
        $userBook2 = new UserBook();
        $status->addUserBook($userBook1);
        $status->addUserBook($userBook2);
        $userBooks = $status->getUserBooks();
        $this->assertCount(2, $userBooks);
        $this->assertContains($userBook1, $userBooks);
        $this->assertContains($userBook2, $userBooks);
    }

    public function testAddUserBook()
    {
        $status = new Status();
        $userBook = new UserBook();
        $status->addUserBook($userBook);
        $this->assertTrue($status->getUserBooks()->contains($userBook));
        $this->assertSame($status, $userBook->getStatus());
    }

    public function testRemoveUserBook()
    {
        $status = new Status();
        $userBook = new UserBook();
        $status->addUserBook($userBook);
        $status->removeUserBook($userBook);
        $this->assertFalse($status->getUserBooks()->contains($userBook));
        $this->assertNull($userBook->getStatus());
    }
}