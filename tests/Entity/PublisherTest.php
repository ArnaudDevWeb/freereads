<?php

namespace App\Tests\Entity;

use App\Entity\Book;
use App\Entity\Publisher;
use PHPUnit\Framework\TestCase;

class PublisherTest extends TestCase
{
    public function testGetBooksReturnsCollection()
    {
        $publisher = new Publisher();
        $this->assertInstanceOf('Doctrine\Common\Collections\Collection', $publisher->getBooks());
    }

    public function testAddBookAddsBookToCollection()
    {
        $publisher = new Publisher();
        $book = new Book();
        $publisher->addBook($book);
        $this->assertTrue($publisher->getBooks()->contains($book));
    }

    public function testAddBookDoesNotAddDuplicateBookToCollection()
    {
        $publisher = new Publisher();
        $book = new Book();
        $publisher->addBook($book);
        $publisher->addBook($book);
        $this->assertCount(1, $publisher->getBooks());
    }

    public function testRemoveBookRemovesBookFromCollection()
    {
        $publisher = new Publisher();
        $book = new Book();
        $publisher->addBook($book);
        $publisher->removeBook($book);
        $this->assertFalse($publisher->getBooks()->contains($book));
    }

    public function testRemoveBookDoesNotRemoveNonexistentBookFromCollection()
    {
        $publisher = new Publisher();
        $book = new Book();
        $publisher->removeBook($book);
        $this->assertCount(0, $publisher->getBooks());
    }
}