<?php
namespace App\Tests\Entity;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Publisher;
use App\Entity\UserBook;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;

class BookTest extends TestCase
{
    public function testGettersAndSetters(): void
    {
        $book = new Book();

        $book->setGoogleBooksId('google_books_id');
        $this->assertEquals('google_books_id', $book->getGoogleBooksId());

        $book->setTitle('title');
        $this->assertEquals('title', $book->getTitle());

        $book->setSubtitle('subtitle');
        $this->assertEquals('subtitle', $book->getSubtitle());

        $publishDate = new DateTime();
        $book->setPublishDate($publishDate);
        $this->assertEquals($publishDate, $book->getPublishDate());

        $book->setDescritpion('description');
        $this->assertEquals('description', $book->getDescritpion());

        $book->setIsbn10('isbn10');
        $this->assertEquals('isbn10', $book->getIsbn10());

        $book->setIsbn13('isbn13');
        $this->assertEquals('isbn13', $book->getIsbn13());

        $book->setPageCount(100);
        $this->assertEquals(100, $book->getPageCount());

        $book->setSmallThumbnail('small_thumbnail');
        $this->assertEquals('small_thumbnail', $book->getSmallThumbnail());

        $book->setThumbnail('thumbnail');
        $this->assertEquals('thumbnail', $book->getThumbnail());
    }

    public function testAuthors(): void
    {
        $book = new Book();
        $author1 = new Author();
        $author2 = new Author();

        $book->addAuthor($author1);
        $this->assertTrue($book->getAuthors()->contains($author1));
        $this->assertFalse($book->getAuthors()->contains($author2));

        $book->addAuthor($author2);
        $this->assertTrue($book->getAuthors()->contains($author2));

        $book->removeAuthor($author1);
        $this->assertFalse($book->getAuthors()->contains($author1));
    }

    public function testPublishers(): void
    {
        $book = new Book();
        $publisher1 = new Publisher();
        $publisher2 = new Publisher();

        $book->addPublisher($publisher1);
        $this->assertTrue($book->getPublishers()->contains($publisher1));
        $this->assertFalse($book->getPublishers()->contains($publisher2));

        $book->addPublisher($publisher2);
        $this->assertTrue($book->getPublishers()->contains($publisher2));

        $book->removePublisher($publisher1);
        $this->assertFalse($book->getPublishers()->contains($publisher1));
    }

    public function testStatus(): void
    {
        $book = new Book();
        $userBook1 = new UserBook();
        $userBook2 = new UserBook();

        $book->addStatus($userBook1);
        $this->assertTrue($book->getStatus()->contains($userBook1));
        $this->assertFalse($book->getStatus()->contains($userBook2));
        $this->assertEquals($book, $userBook1->getBook());

        $book->addStatus($userBook2);
        $this->assertTrue($book->getStatus()->contains($userBook2));
        $this->assertEquals($book, $userBook2->getBook());

        $book->removeStatus($userBook1);
        $this->assertFalse($book->getStatus()->contains($userBook1));
        $this->assertNull($userBook1->getBook());
    }

    public function testUserBooks(): void
    {
        $book = new Book();
        $userBook1 = new UserBook();
        $userBook2 = new UserBook();

        $book->addUserBook($userBook1);
        $this->assertTrue($book->getUserBooks()->contains($userBook1));
        $this->assertFalse($book->getUserBooks()->contains($userBook2));
        $this->assertEquals($book, $userBook1->getBook());

        $book->addUserBook($userBook2);
        $this->assertTrue($book->getUserBooks()->contains($userBook2));
        $this->assertEquals($book, $userBook2->getBook());

        $book->removeUserBook($userBook1);
        $this->assertFalse($book->getUserBooks()->contains($userBook1));
        $this->assertNull($userBook1->getBook());
    }
}