<?php

namespace App\Tests\Entity;

use App\Entity\Author;
use App\Entity\Book;
use PHPUnit\Framework\TestCase;

class AuthorTest extends TestCase
{
    public function testGetId(): void
    {
        $author = new Author();
        $this->assertNull($author->getId());
    }

    public function testGetName(): void
    {
        $author = new Author();
        $author->setName('John Doe');
        $this->assertSame('John Doe', $author->getName());
    }

    public function testGetBooks(): void
    {
        $author = new Author();
        $book1 = new Book();
        $book2 = new Book();
        $author->addBook($book1);
        $author->addBook($book2);
        $this->assertCount(2, $author->getBooks());
        $this->assertTrue($author->getBooks()->contains($book1));
        $this->assertTrue($author->getBooks()->contains($book2));
    }

    public function testAddBook(): void
    {
        $author = new Author();
        $book = new Book();
        $author->addBook($book);
        $this->assertCount(1, $author->getBooks());
        $this->assertTrue($author->getBooks()->contains($book));
        $this->assertTrue($book->getAuthors()->contains($author));
    }

    public function testRemoveBook(): void
    {
        $author = new Author();
        $book = new Book();
        $author->addBook($book);
        $author->removeBook($book);
        $this->assertCount(0, $author->getBooks());
        $this->assertFalse($author->getBooks()->contains($book));
        $this->assertFalse($book->getAuthors()->contains($author));
    }
}